package org.jeecg.modules.demo.test.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.base.controller.XuRuiController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.demo.test.entity.XuRuiOrderCustomer;
import org.jeecg.modules.demo.test.entity.XuRuiOrderMain;
import org.jeecg.modules.demo.test.entity.XuRuiOrderTicket;
import org.jeecg.modules.demo.test.service.IXuRuiOrderCustomerService;
import org.jeecg.modules.demo.test.service.IXuRuiOrderMainService;
import org.jeecg.modules.demo.test.service.IXuRuiOrderTicketService;
import org.jeecg.modules.demo.test.vo.XuRuiOrderMainPage;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description: 一对多示例（JEditableTable行编辑）
 * @Author: xurui-boot
 * @Date:2019-02-15
 * @Version: V2.0
 */
@RestController
@RequestMapping("/test/xuRuiOrderMain")
@Slf4j
public class XuRuiOrderMainController extends XuRuiController<XuRuiOrderMain, IXuRuiOrderMainService> {

    @Autowired
    private IXuRuiOrderMainService xuRuiOrderMainService;
    @Autowired
    private IXuRuiOrderCustomerService xuRuiOrderCustomerService;
    @Autowired
    private IXuRuiOrderTicketService xuRuiOrderTicketService;

    /**
     * 分页列表查询
     *
     * @param xuRuiOrderMain
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @GetMapping(value = "/list")
    public Result<?> queryPageList(XuRuiOrderMain xuRuiOrderMain, @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo, @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize, HttpServletRequest req) {
        QueryWrapper<XuRuiOrderMain> queryWrapper = QueryGenerator.initQueryWrapper(xuRuiOrderMain, req.getParameterMap());
        Page<XuRuiOrderMain> page = new Page<XuRuiOrderMain>(pageNo, pageSize);
        IPage<XuRuiOrderMain> pageList = xuRuiOrderMainService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

    /**
     * 添加
     *
     * @param xuRuiOrderMainPage
     * @return
     */
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody XuRuiOrderMainPage xuRuiOrderMainPage) {
        XuRuiOrderMain xuRuiOrderMain = new XuRuiOrderMain();
        BeanUtils.copyProperties(xuRuiOrderMainPage, xuRuiOrderMain);
        xuRuiOrderMainService.saveMain(xuRuiOrderMain, xuRuiOrderMainPage.getXuRuiOrderCustomerList(), xuRuiOrderMainPage.getXuRuiOrderTicketList());
        return Result.ok("添加成功！");
    }

    /**
     * 编辑
     *
     * @param xuRuiOrderMainPage
     * @return
     */
    @PutMapping(value = "/edit")
    public Result<?> eidt(@RequestBody XuRuiOrderMainPage xuRuiOrderMainPage) {
        XuRuiOrderMain xuRuiOrderMain = new XuRuiOrderMain();
        BeanUtils.copyProperties(xuRuiOrderMainPage, xuRuiOrderMain);
        xuRuiOrderMainService.updateCopyMain(xuRuiOrderMain, xuRuiOrderMainPage.getXuRuiOrderCustomerList(), xuRuiOrderMainPage.getXuRuiOrderTicketList());
        return Result.ok("编辑成功！");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        xuRuiOrderMainService.delMain(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.xuRuiOrderMainService.delBatchMain(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        XuRuiOrderMain xuRuiOrderMain = xuRuiOrderMainService.getById(id);
        return Result.ok(xuRuiOrderMain);
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/queryOrderCustomerListByMainId")
    public Result<?> queryOrderCustomerListByMainId(@RequestParam(name = "id", required = true) String id) {
        List<XuRuiOrderCustomer> xuRuiOrderCustomerList = xuRuiOrderCustomerService.selectCustomersByMainId(id);
        return Result.ok(xuRuiOrderCustomerList);
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/queryOrderTicketListByMainId")
    public Result<?> queryOrderTicketListByMainId(@RequestParam(name = "id", required = true) String id) {
        List<XuRuiOrderTicket> xuRuiOrderTicketList = xuRuiOrderTicketService.selectTicketsByMainId(id);
        return Result.ok(xuRuiOrderTicketList);
    }

    /**
     * 导出excel
     *
     * @param request
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, XuRuiOrderMain xuRuiOrderMain) {
        // Step.1 组装查询条件
        QueryWrapper<XuRuiOrderMain> queryWrapper = QueryGenerator.initQueryWrapper(xuRuiOrderMain, request.getParameterMap());
        //Step.2 AutoPoi 导出Excel
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        //获取当前用户
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

        List<XuRuiOrderMainPage> pageList = new ArrayList<XuRuiOrderMainPage>();

        List<XuRuiOrderMain> xuRuiOrderMainList = xuRuiOrderMainService.list(queryWrapper);
        for (XuRuiOrderMain orderMain : xuRuiOrderMainList) {
            XuRuiOrderMainPage vo = new XuRuiOrderMainPage();
            BeanUtils.copyProperties(orderMain, vo);
            // 查询机票
            List<XuRuiOrderTicket> xuRuiOrderTicketList = xuRuiOrderTicketService.selectTicketsByMainId(orderMain.getId());
            vo.setXuRuiOrderTicketList(xuRuiOrderTicketList);
            // 查询客户
            List<XuRuiOrderCustomer> xuRuiOrderCustomerList = xuRuiOrderCustomerService.selectCustomersByMainId(orderMain.getId());
            vo.setXuRuiOrderCustomerList(xuRuiOrderCustomerList);
            pageList.add(vo);
        }

        // 导出文件名称
        mv.addObject(NormalExcelConstants.FILE_NAME, "一对多订单示例");
        // 注解对象Class
        mv.addObject(NormalExcelConstants.CLASS, XuRuiOrderMainPage.class);
        // 自定义表格参数
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("自定义导出Excel内容标题", "导出人:" + sysUser.getRealname(), "自定义Sheet名字"));
        // 导出数据列表
        mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
        return mv;
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
            MultipartFile file = entity.getValue();// 获取上传文件对象
            ImportParams params = new ImportParams();
            params.setTitleRows(2);
            params.setHeadRows(2);
            params.setNeedSave(true);
            try {
                List<XuRuiOrderMainPage> list = ExcelImportUtil.importExcel(file.getInputStream(), XuRuiOrderMainPage.class, params);
                for (XuRuiOrderMainPage page : list) {
                    XuRuiOrderMain po = new XuRuiOrderMain();
                    BeanUtils.copyProperties(page, po);
                    xuRuiOrderMainService.saveMain(po, page.getXuRuiOrderCustomerList(), page.getXuRuiOrderTicketList());
                }
                return Result.ok("文件导入成功！");
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                return Result.error("文件导入失败：" + e.getMessage());
            } finally {
                try {
                    file.getInputStream().close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return Result.error("文件导入失败！");
    }

}

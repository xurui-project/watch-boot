package org.jeecg.modules.demo.pay.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.pay.entity.PayConfig;
import org.jeecg.modules.demo.pay.service.IPayConfigService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.XuRuiController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: pay_config
 * @Author: xurui-boot
 * @Date:   2022-01-17
 * @Version: V1.0
 */
@Api(tags="pay_config")
@RestController
@RequestMapping("/pay/payConfig")
@Slf4j
public class PayConfigController extends XuRuiController<PayConfig, IPayConfigService> {
	@Autowired
	private IPayConfigService payConfigService;
	
	/**
	 * 分页列表查询
	 *
	 * @param payConfig
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "pay_config-分页列表查询")
	@ApiOperation(value="pay_config-分页列表查询", notes="pay_config-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(PayConfig payConfig,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<PayConfig> queryWrapper = QueryGenerator.initQueryWrapper(payConfig, req.getParameterMap());
		Page<PayConfig> page = new Page<PayConfig>(pageNo, pageSize);
		IPage<PayConfig> pageList = payConfigService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param payConfig
	 * @return
	 */
	@AutoLog(value = "pay_config-添加")
	@ApiOperation(value="pay_config-添加", notes="pay_config-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody PayConfig payConfig) {
		payConfigService.save(payConfig);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param payConfig
	 * @return
	 */
	@AutoLog(value = "pay_config-编辑")
	@ApiOperation(value="pay_config-编辑", notes="pay_config-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody PayConfig payConfig) {
		payConfigService.updateById(payConfig);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "pay_config-通过id删除")
	@ApiOperation(value="pay_config-通过id删除", notes="pay_config-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		payConfigService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "pay_config-批量删除")
	@ApiOperation(value="pay_config-批量删除", notes="pay_config-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.payConfigService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "pay_config-通过id查询")
	@ApiOperation(value="pay_config-通过id查询", notes="pay_config-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		PayConfig payConfig = payConfigService.getById(id);
		if(payConfig==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(payConfig);
	}

	 @ApiOperation(value = "获取Applink", notes = "获取Applink")
	 @GetMapping(value = "getAppLinks/{payId}")
	 public String getApplinks(@PathVariable Long payId) {
		 PayConfig payConfig = payConfigService.getById(payId);
		 return payConfig.getApplinks();
	 }

	 @ApiOperation(value = "支付方式-启用状态", notes = "支付方式-启用状态")
	 @RequestMapping(value = "/setPayStatus", method = RequestMethod.PUT)
	 public Result<?> setPayStatus(@RequestBody JSONObject jsonObject) {
		 String ids = jsonObject.getString("ids");
		 Integer payStatus =Integer.parseInt(jsonObject.getString("payStatus"));
		 return payConfigService.setPayStatus(ids, payStatus);
	 }

	 @ApiOperation(value = "支付方式-修改测试状态", notes = "支付方式-修改测试状态")
	 @RequestMapping(value = "/setIsTest", method = RequestMethod.PUT)
	 public Result<?> setIsTest(@RequestBody JSONObject jsonObject) {
		 String ids = jsonObject.getString("ids");
		 Integer isTest =Integer.parseInt(jsonObject.getString("isTest"));
		 return payConfigService.setIsTest(ids, isTest);
	 }

    /**
    * 导出excel
    *
    * @param request
    * @param payConfig
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, PayConfig payConfig) {
        return super.exportXls(request, payConfig, PayConfig.class, "pay_config");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, PayConfig.class);
    }

}

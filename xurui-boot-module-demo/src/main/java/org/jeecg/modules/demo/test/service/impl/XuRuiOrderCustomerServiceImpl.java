package org.jeecg.modules.demo.test.service.impl;

import java.util.List;

import org.jeecg.modules.demo.test.entity.XuRuiOrderCustomer;
import org.jeecg.modules.demo.test.mapper.XuRuiOrderCustomerMapper;
import org.jeecg.modules.demo.test.service.IXuRuiOrderCustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 订单客户
 * @Author: xurui-boot
 * @Date:  2019-02-15
 * @Version: V1.0
 */
@Service
public class XuRuiOrderCustomerServiceImpl extends ServiceImpl<XuRuiOrderCustomerMapper, XuRuiOrderCustomer> implements IXuRuiOrderCustomerService {

	@Autowired
	private XuRuiOrderCustomerMapper xuRuiOrderCustomerMapper;
	
	@Override
	public List<XuRuiOrderCustomer> selectCustomersByMainId(String mainId) {
		return xuRuiOrderCustomerMapper.selectCustomersByMainId(mainId);
	}

}

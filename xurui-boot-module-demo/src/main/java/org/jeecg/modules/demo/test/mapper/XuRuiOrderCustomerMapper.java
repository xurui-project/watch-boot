package org.jeecg.modules.demo.test.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.demo.test.entity.XuRuiOrderCustomer;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 订单客户
 * @Author: xurui-boot
 * @Date:  2019-02-15
 * @Version: V1.0
 */
public interface XuRuiOrderCustomerMapper extends BaseMapper<XuRuiOrderCustomer> {
	
	/**
	 *  通过主表外键批量删除客户
	 * @param mainId
	 * @return
	 */
    @Delete("DELETE FROM xurui_order_customer WHERE order_id = #{mainId}")
	public boolean deleteCustomersByMainId(String mainId);
    
    @Select("SELECT * FROM xurui_order_customer WHERE order_id = #{mainId}")
	public List<XuRuiOrderCustomer> selectCustomersByMainId(String mainId);
}

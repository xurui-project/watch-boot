package org.jeecg.modules.demo.pay.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.demo.pay.entity.PayConfig;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.demo.pay.response.PayResponse;

/**
 * @Description: pay_config
 * @Author: xurui-boot
 * @Date:   2022-01-17
 * @Version: V1.0
 */
public interface IPayConfigService extends IService<PayConfig> {

    /**
     * 获取支付响应对象
     *
     * @param payId
     * @param handlerName
     * @return
     */
    PayResponse getPayResponse(String payId, String handlerName);

    /**
     * 是否启用
     * @param ids
     * @param payStatus
     * @return
     */
    public Result<PayConfig> setPayStatus(String ids,Integer payStatus);

    /**
     * 是否测试
     * @param ids
     * @param isTest
     * @return
     */
    public Result<PayConfig> setIsTest(String ids,Integer isTest);
}

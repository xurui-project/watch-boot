package org.jeecg.modules.demo.device.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.demo.device.entity.DeviceConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: device_config
 * @Author: xurui-boot
 * @Date:   2022-01-18
 * @Version: V1.0
 */
public interface DeviceConfigMapper extends BaseMapper<DeviceConfig> {

    public  DeviceConfig quertyConfigInfo();

    public void updateDelFlag(@Param("id") String id);
}

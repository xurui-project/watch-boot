package org.jeecg.modules.demo.device.controller;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.demo.device.entity.DeviceCustomer;
import org.jeecg.modules.demo.device.model.TreeModel;
import org.jeecg.modules.demo.device.service.IDeviceCustomerService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.XuRuiController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: device_customer
 * @Author: xurui-boot
 * @Date:   2022-01-18
 * @Version: V1.0
 */
@Api(tags="device_customer")
@RestController
@RequestMapping("/device/deviceCustomer")
@Slf4j
public class DeviceCustomerController extends XuRuiController<DeviceCustomer, IDeviceCustomerService> {
	@Autowired
	private IDeviceCustomerService deviceCustomerService;
	
	/**
	 * 分页列表查询
	 *
	 * @param deviceCustomer
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "device_customer-分页列表查询")
	@ApiOperation(value="device_customer-分页列表查询", notes="device_customer-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(DeviceCustomer deviceCustomer,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<DeviceCustomer> queryWrapper = QueryGenerator.initQueryWrapper(deviceCustomer, req.getParameterMap());
		Page<DeviceCustomer> page = new Page<DeviceCustomer>(pageNo, pageSize);
		IPage<DeviceCustomer> pageList = deviceCustomerService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param deviceCustomer
	 * @return
	 */
	@AutoLog(value = "device_customer-添加")
	@ApiOperation(value="device_customer-添加", notes="device_customer-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody DeviceCustomer deviceCustomer) {
		deviceCustomerService.save(deviceCustomer);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param deviceCustomer
	 * @return
	 */
	@AutoLog(value = "device_customer-编辑")
	@ApiOperation(value="device_customer-编辑", notes="device_customer-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody DeviceCustomer deviceCustomer) {
		deviceCustomerService.updateById(deviceCustomer);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "device_customer-通过id删除")
	@ApiOperation(value="device_customer-通过id删除", notes="device_customer-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		//deviceCustomerService.removeById(id);
		deviceCustomerService.updateDelFlag(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "device_customer-批量删除")
	@ApiOperation(value="device_customer-批量删除", notes="device_customer-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.deviceCustomerService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "device_customer-通过id查询")
	@ApiOperation(value="device_customer-通过id查询", notes="device_customer-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		DeviceCustomer deviceCustomer = deviceCustomerService.getById(id);
		if(deviceCustomer==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(deviceCustomer);
	}
	 /**
	  * 查找客户名称树
	  * @param request
	  * @return
	  */
	 @RequestMapping(value = "/queryTreeList", method = RequestMethod.GET)
	 public Result<Map<String,Object>> queryTreeList(HttpServletRequest request) {
		 Result<Map<String,Object>> result = new Result<>();
		 //全部权限ids
		 List<String> ids = new ArrayList<>();
		 String customerName=request.getParameter("customerName");
		 try {
			 LambdaQueryWrapper<DeviceCustomer> query = new LambdaQueryWrapper<DeviceCustomer>();

			 if(customerName!=null){
				 query.like(DeviceCustomer::getCustomerName,customerName);
			 }

			 query.eq(DeviceCustomer::getDelFlag,"0");
			 List<DeviceCustomer> list = deviceCustomerService.list(query);
			 List<TreeModel> treeList = new ArrayList<>();

			 for (DeviceCustomer permission : list) {
				 String tempPid = "";
				 TreeModel tree = new TreeModel(permission.getId(), tempPid, permission.getCustomerName(),1, true);
				 tree.setKey(permission.getId());
				 tree.setValue(permission.getId());
				 tree.setTitle(permission.getCustomerName());
				 treeList.add(tree);
			 }
			 Map<String,Object> resMap = new HashMap<String,Object>();
			 resMap.put("treeList", treeList); //全部树节点数据
			 result.setResult(resMap);
			 result.setSuccess(true);
		 } catch (Exception e) {
			 log.error(e.getMessage(), e);
		 }
		 return result;
	 }

    /**
    * 导出excel
    *
    * @param request
    * @param deviceCustomer
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, DeviceCustomer deviceCustomer) {
        return super.exportXls(request, deviceCustomer, DeviceCustomer.class, "device_customer");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, DeviceCustomer.class);
    }

}

package org.jeecg.modules.demo.device.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: device_activation
 * @Author: xurui-boot
 * @Date:   2022-01-18
 * @Version: V1.0
 */
@Data
@TableName("device_activation")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="device_activation对象", description="device_activation")
public class DeviceActivation implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键ID*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private String id;
	/**设备ID*/
	@Excel(name = "设备ID", width = 15)
    @ApiModelProperty(value = "设备ID")
    private String deviceId;
	/**使用状态(0.未试用、1.已试用、2.已激活)*/
	@Excel(name = "使用状态(0.未试用、1.已试用、2.已激活)", width = 15)
    @ApiModelProperty(value = "使用状态(0.未试用、1.已试用、2.已激活)")
    @Dict(dicCode = "useStatus")
    private String useStatus;
	/**试用开始时间*/
	@Excel(name = "试用开始时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "试用开始时间")
    private Date trialStartTime;
	/**试用结束时间*/
	@Excel(name = "试用结束时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "试用结束时间")
    private Date trialEndTime;
	/**试用时长*/
	@Excel(name = "试用时长", width = 15)
    @ApiModelProperty(value = "试用时长")
    private Integer trialDuration;
	/**激活开始时间*/
	@Excel(name = "激活开始时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "激活开始时间")
    private Date activationStartTime;
	/**激活结束时间*/
	@Excel(name = "激活结束时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "激活结束时间")
    private Date activationEndTime;
	/**激活时长*/
	@Excel(name = "激活时长", width = 15)
    @ApiModelProperty(value = "激活时长")
    private Integer activationDuration;
	/**客户ID*/
	@Excel(name = "客户ID", width = 15)
    @ApiModelProperty(value = "客户ID")
    @Dict(dictTable ="device_customer",dicText = "customer_name",dicCode = "id")
    private String customerId;
	/**安卓费用*/
	@Excel(name = "安卓费用", width = 15)
    @ApiModelProperty(value = "安卓费用")
    private BigDecimal androidCost;
	/**IOS费用*/
	@Excel(name = "IOS费用", width = 15)
    @ApiModelProperty(value = "IOS费用")
    private BigDecimal iosCost;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
    /**
     * 删除状态（0，正常，1已删除）
     */
    @Excel(name = "删除状态", width = 15,dicCode="del_flag")
    @TableLogic
    private String delFlag;
}

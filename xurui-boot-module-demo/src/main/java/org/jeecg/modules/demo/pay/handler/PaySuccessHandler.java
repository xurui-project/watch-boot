package org.jeecg.modules.demo.pay.handler;

import com.egzosn.pay.common.api.PayMessageHandler;
import com.egzosn.pay.common.api.PayMessageInterceptor;
import com.egzosn.pay.common.api.PayService;
import com.egzosn.pay.common.bean.PayMessage;
import com.egzosn.pay.common.bean.PayOutMessage;
import com.egzosn.pay.common.exception.PayErrorException;
import com.google.common.collect.ImmutableSet;
import org.jeecg.modules.demo.pay.enums.PayType;

import java.util.Map;
import java.util.Set;

/**
 * 支付处理器接口
 *
 * @author zyf
 */
public interface PaySuccessHandler extends PayMessageInterceptor<PayMessage, PayService>, PayMessageHandler<PayMessage, PayService> {

    Set<String> TRADE_STATUSES = ImmutableSet.of("TRADE_SUCCESS", "TRADE_FINISHED");

    /**
     * 自定义业务校验,比如验证是否已支付
     *
     * @param payMessage 支付回调消息
     * @param context    上下文参数,可传递到success方法内
     * @param payService 支付服务
     */
    boolean validate(PayMessage payMessage, Map<String, Object> context, PayService payService, String outTradeNo);

    /**
     * 自定义支付成功回调
     *
     * @param payMessage 支付回调消息
     * @param outTradeNo 订单号
     * @param context    上下文参数
     * @param payService 支付服务
     * @return
     */
    boolean success(PayMessage payMessage, String outTradeNo, Map<String, Object> context, PayService payService);

    /**
     * 支付回调成功前处理
     *
     * @param payMessage
     * @param context
     * @param payService
     * @return
     * @throws PayErrorException
     */
    @Override
    default PayOutMessage handle(PayMessage payMessage, Map<String, Object> context, PayService payService) throws PayErrorException {
        String payCode = String.valueOf(payService.getPayConfigStorage().getPayType());
        Map<String, Object> message = payMessage.getPayMessage();
        String fromPay = payMessage.getFromPay();
        String outTradeNo = payMessage.getOutTradeNo();
        String payType = payMessage.getPayType();
        String transactionType = payMessage.getTransactionType();
        context.put("fromPay", fromPay);
        context.put("transactionType", transactionType);
        context.put("outTradeNo", outTradeNo);
        context.put("payType", payType);
        //支付宝
        if (PayType.aliPay.name().equals(payCode)) {
            //交易状态
            String trade_status = (String) message.get("trade_status");
            //支付成功或支付完成
            if (TRADE_STATUSES.contains(trade_status)) {
                Boolean tag = success(payMessage, outTradeNo, context, payService);
                if (tag) {
                    return payService.getPayOutMessage("success", "成功");
                }
            }
        }
        //微信支付
        if (PayType.wxPay.name().equals(payCode)) {
            //交易状态
            String result_code = (String) message.get("result_code");
            if ("SUCCESS".equals(result_code)) {
                success(payMessage, outTradeNo, context, payService);
                return payService.getPayOutMessage("SUCCESS", "成功");
            } else {
                return payService.getPayOutMessage("SUCCESS", "失败");
            }
        }
        return payService.getPayOutMessage("fail", "失败");
    }

    /**
     * 支付回调前校验
     *
     * @param payMessage
     * @param context
     * @param payService
     * @return
     * @throws PayErrorException
     */
    @Override
    default boolean intercept(PayMessage payMessage, Map<String, Object> context, PayService payService) throws PayErrorException {
        String outTradeNo = payMessage.getOutTradeNo();
        return validate(payMessage, context, payService, outTradeNo);
    }
}

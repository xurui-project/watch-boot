package org.jeecg.modules.demo.test.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.demo.test.entity.XuRuiOrderCustomer;
import org.jeecg.modules.demo.test.entity.XuRuiOrderMain;
import org.jeecg.modules.demo.test.entity.XuRuiOrderTicket;
import org.jeecg.modules.demo.test.service.IXuRuiOrderCustomerService;
import org.jeecg.modules.demo.test.service.IXuRuiOrderMainService;
import org.jeecg.modules.demo.test.service.IXuRuiOrderTicketService;
import org.jeecg.modules.demo.test.vo.XuRuiOrderMainPage;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description: 一对多示例（ERP TAB风格）
 * @Author: ZhiLin
 * @Date: 2019-02-20
 * @Version: v2.0
 */
@Slf4j
@RestController
@RequestMapping("/test/order")
public class XuRuiOrderTabMainController {

    @Autowired
    private IXuRuiOrderMainService xuRuiOrderMainService;
    @Autowired
    private IXuRuiOrderCustomerService xuRuiOrderCustomerService;
    @Autowired
    private IXuRuiOrderTicketService xuRuiOrderTicketService;

    /**
     * 分页列表查询
     *
     * @param xuRuiOrderMain
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @GetMapping(value = "/orderList")
    public Result<?> respondePagedData(XuRuiOrderMain xuRuiOrderMain,
                                       @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                       @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                       HttpServletRequest req) {
        QueryWrapper<XuRuiOrderMain> queryWrapper = QueryGenerator.initQueryWrapper(xuRuiOrderMain, req.getParameterMap());
        Page<XuRuiOrderMain> page = new Page<XuRuiOrderMain>(pageNo, pageSize);
        IPage<XuRuiOrderMain> pageList = xuRuiOrderMainService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

    /**
     * 添加
     *
     * @param xuRuiOrderMainPage
     * @return
     */
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody XuRuiOrderMainPage xuRuiOrderMainPage) {
        XuRuiOrderMain xuRuiOrderMain = new XuRuiOrderMain();
        BeanUtils.copyProperties(xuRuiOrderMainPage, xuRuiOrderMain);
        xuRuiOrderMainService.save(xuRuiOrderMain);
        return Result.ok("添加成功!");
    }

    /**
     * 编辑
     *
     * @param xuRuiOrderMainPage
     * @return
     */
    @PutMapping("/edit")
    public Result<?> edit(@RequestBody XuRuiOrderMainPage xuRuiOrderMainPage) {
        XuRuiOrderMain xuRuiOrderMain = new XuRuiOrderMain();
        BeanUtils.copyProperties(xuRuiOrderMainPage, xuRuiOrderMain);
        xuRuiOrderMainService.updateById(xuRuiOrderMain);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        xuRuiOrderMainService.delMain(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.xuRuiOrderMainService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        XuRuiOrderMain xuRuiOrderMain = xuRuiOrderMainService.getById(id);
        return Result.ok(xuRuiOrderMain);
    }


    /**
     * 通过id查询
     *
     * @param xuRuiOrderCustomer
     * @return
     */
    @GetMapping(value = "/listOrderCustomerByMainId")
    public Result<?> queryOrderCustomerListByMainId(XuRuiOrderCustomer xuRuiOrderCustomer,
                                                    @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                    HttpServletRequest req) {
        QueryWrapper<XuRuiOrderCustomer> queryWrapper = QueryGenerator.initQueryWrapper(xuRuiOrderCustomer, req.getParameterMap());
        Page<XuRuiOrderCustomer> page = new Page<XuRuiOrderCustomer>(pageNo, pageSize);
        IPage<XuRuiOrderCustomer> pageList = xuRuiOrderCustomerService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

    /**
     * 通过id查询
     *
     * @param xuRuiOrderTicket
     * @return
     */
    @GetMapping(value = "/listOrderTicketByMainId")
    public Result<?> queryOrderTicketListByMainId(XuRuiOrderTicket xuRuiOrderTicket,
                                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                  @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                  HttpServletRequest req) {
        QueryWrapper<XuRuiOrderTicket> queryWrapper = QueryGenerator.initQueryWrapper(xuRuiOrderTicket, req.getParameterMap());
        Page<XuRuiOrderTicket> page = new Page<XuRuiOrderTicket>(pageNo, pageSize);
        IPage<XuRuiOrderTicket> pageList = xuRuiOrderTicketService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

    /**
     * 添加
     *
     * @param xuRuiOrderCustomer
     * @return
     */
    @PostMapping(value = "/addCustomer")
    public Result<?> addCustomer(@RequestBody XuRuiOrderCustomer xuRuiOrderCustomer) {
        xuRuiOrderCustomerService.save(xuRuiOrderCustomer);
        return Result.ok("添加成功!");
    }

    /**
     * 编辑
     *
     * @param xuRuiOrderCustomer
     * @return
     */
    @PutMapping("/editCustomer")
    public Result<?> editCustomer(@RequestBody XuRuiOrderCustomer xuRuiOrderCustomer) {
        xuRuiOrderCustomerService.updateById(xuRuiOrderCustomer);
        return Result.ok("添加成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/deleteCustomer")
    public Result<?> deleteCustomer(@RequestParam(name = "id", required = true) String id) {
        xuRuiOrderCustomerService.removeById(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @DeleteMapping(value = "/deleteBatchCustomer")
    public Result<?> deleteBatchCustomer(@RequestParam(name = "ids", required = true) String ids) {
        this.xuRuiOrderCustomerService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 添加
     *
     * @param xuRuiOrderTicket
     * @return
     */
    @PostMapping(value = "/addTicket")
    public Result<?> addTicket(@RequestBody XuRuiOrderTicket xuRuiOrderTicket) {
        xuRuiOrderTicketService.save(xuRuiOrderTicket);
        return Result.ok("添加成功!");
    }

    /**
     * 编辑
     *
     * @param xuRuiOrderTicket
     * @return
     */
    @PutMapping("/editTicket")
    public Result<?> editTicket(@RequestBody XuRuiOrderTicket xuRuiOrderTicket) {
        xuRuiOrderTicketService.updateById(xuRuiOrderTicket);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/deleteTicket")
    public Result<?> deleteTicket(@RequestParam(name = "id", required = true) String id) {
        xuRuiOrderTicketService.removeById(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @DeleteMapping(value = "/deleteBatchTicket")
    public Result<?> deleteBatchTicket(@RequestParam(name = "ids", required = true) String ids) {
        this.xuRuiOrderTicketService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

}
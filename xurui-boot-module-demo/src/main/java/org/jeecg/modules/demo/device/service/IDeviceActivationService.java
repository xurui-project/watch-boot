package org.jeecg.modules.demo.device.service;

import org.jeecg.modules.demo.device.entity.DeviceActivation;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Date;
import java.util.List;

/**
 * @Description: device_activation
 * @Author: xurui-boot
 * @Date:   2022-01-18
 * @Version: V1.0
 */
public interface IDeviceActivationService extends IService<DeviceActivation> {
    /**
     * 根据设备ID、设备终端类型返回设备终端对应信息
     * @param deviceId
     * @return
     */
    public DeviceActivation deviceStatus(String deviceId);

    public int countTrialEnd(String trialEndTime,String activationEndTime);

    public List<DeviceActivation> getCustomerList(String customerId);

    public void updateDelFlag(String id);
}

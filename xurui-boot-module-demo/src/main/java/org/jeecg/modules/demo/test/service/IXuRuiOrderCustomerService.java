package org.jeecg.modules.demo.test.service;

import java.util.List;

import org.jeecg.modules.demo.test.entity.XuRuiOrderCustomer;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 订单客户
 * @Author: xurui-boot
 * @Date:  2019-02-15
 * @Version: V1.0
 */
public interface IXuRuiOrderCustomerService extends IService<XuRuiOrderCustomer> {
	
	public List<XuRuiOrderCustomer> selectCustomersByMainId(String mainId);
}

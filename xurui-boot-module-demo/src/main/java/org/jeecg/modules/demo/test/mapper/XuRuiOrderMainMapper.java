package org.jeecg.modules.demo.test.mapper;

import org.jeecg.modules.demo.test.entity.XuRuiOrderMain;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 订单
 * @Author: xurui-boot
 * @Date:  2019-02-15
 * @Version: V1.0
 */
public interface XuRuiOrderMainMapper extends BaseMapper<XuRuiOrderMain> {

}

package org.jeecg.modules.demo.test.service;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.jeecg.modules.demo.test.entity.XuRuiOrderCustomer;
import org.jeecg.modules.demo.test.entity.XuRuiOrderMain;
import org.jeecg.modules.demo.test.entity.XuRuiOrderTicket;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 订单
 * @Author: xurui-boot
 * @Date:  2019-02-15
 * @Version: V1.0
 */
public interface IXuRuiOrderMainService extends IService<XuRuiOrderMain> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(XuRuiOrderMain xuRuiOrderMain,List<XuRuiOrderCustomer> xuRuiOrderCustomerList,List<XuRuiOrderTicket> xuRuiOrderTicketList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(XuRuiOrderMain xuRuiOrderMain,List<XuRuiOrderCustomer> xuRuiOrderCustomerList,List<XuRuiOrderTicket> jxuRuiOrderTicketList);
	
	/**
	 * 删除一对多
	 * @param jformOrderMain
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 * @param jformOrderMain
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);

	public void updateCopyMain(XuRuiOrderMain xuRuiOrderMain, List<XuRuiOrderCustomer> xuRuiOrderCustomerList, List<XuRuiOrderTicket> xuRuiOrderTicketList);
}

package org.jeecg.modules.demo.pay.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.pay.entity.PayConfig;
import org.jeecg.modules.demo.pay.mapper.PayConfigMapper;
import org.jeecg.modules.demo.pay.response.PayResponse;
import org.jeecg.modules.demo.pay.service.IPayConfigService;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @Description: pay_config
 * @Author: xurui-boot
 * @Date:   2022-01-17
 * @Version: V1.0
 */
@Service
public class PayConfigServiceImpl extends ServiceImpl<PayConfigMapper, PayConfig> implements IPayConfigService {

    @Resource
    public AutowireCapableBeanFactory spring;

    /**
     * 获取支付响应
     *
     * @param id 账户id
     * @return 支付响应
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public PayResponse getPayResponse(String id, String handlerName) {
        PayResponse payResponse = new PayResponse();
        PayConfig payConfig = getById(id);

        spring.autowireBean(payResponse);
        payResponse.init(payConfig, handlerName);
        return payResponse;
    }

    @Override
    public Result<PayConfig> setPayStatus(String ids, Integer payStatus) {
        Result<PayConfig> result = new Result<PayConfig>();
        try {
            String[] arr = ids.split(",");
            for (String id : arr) {
                if(oConvertUtils.isNotEmpty(id)) {
                    PayConfig payConfig=new PayConfig();
                    payConfig.setPayStatus(payStatus);
                    this.update(payConfig,
                            new UpdateWrapper<PayConfig>().lambda().eq(PayConfig::getId,id));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.error500("操作失败"+e.getMessage());
        }
        result.success("操作成功!");
        return result;
    }

    @Override
    public Result<PayConfig> setIsTest(String ids, Integer isTest) {
        Result<PayConfig> result = new Result<PayConfig>();
        try {
            String[] arr = ids.split(",");
            for (String id : arr) {
                if(oConvertUtils.isNotEmpty(id)) {
                    PayConfig payConfig=new PayConfig();
                    if(isTest.intValue()==1){
                        payConfig.setIsTest(Boolean.TRUE);
                    }else{
                        payConfig.setIsTest(Boolean.FALSE);
                    }

                    this.update(payConfig,
                            new UpdateWrapper<PayConfig>().lambda().eq(PayConfig::getId,id));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.error500("操作失败"+e.getMessage());
        }
        result.success("操作成功!");
        return result;
    }
}

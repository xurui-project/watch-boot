package org.jeecg.modules.demo.test.service.impl;

import java.util.List;

import org.jeecg.modules.demo.test.entity.XuRuiOrderTicket;
import org.jeecg.modules.demo.test.mapper.XuRuiOrderTicketMapper;
import org.jeecg.modules.demo.test.service.IXuRuiOrderTicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 订单机票
 * @Author: xurui-boot
 * @Date:  2019-02-15
 * @Version: V1.0
 */
@Service
public class XuRuiOrderTicketServiceImpl extends ServiceImpl<XuRuiOrderTicketMapper, XuRuiOrderTicket> implements IXuRuiOrderTicketService {
	@Autowired
	private XuRuiOrderTicketMapper xuRuiOrderTicketMapper;
	
	@Override
	public List<XuRuiOrderTicket> selectTicketsByMainId(String mainId) {
		return xuRuiOrderTicketMapper.selectTicketsByMainId(mainId);
	}

}

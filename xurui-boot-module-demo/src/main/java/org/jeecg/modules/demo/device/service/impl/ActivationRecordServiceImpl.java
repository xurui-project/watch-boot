package org.jeecg.modules.demo.device.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.device.entity.ActivationRecord;
import org.jeecg.modules.demo.device.entity.DeviceActivation;
import org.jeecg.modules.demo.device.mapper.ActivationRecordMapper;
import org.jeecg.modules.demo.device.service.IActivationRecordService;
import org.jeecg.modules.demo.device.service.IDeviceActivationService;
import org.jeecg.modules.demo.pay.enums.PayStatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * @Description: activation_record
 * @Author: xurui-boot
 * @Date:   2022-01-18
 * @Version: V1.0
 */
@Service
public class ActivationRecordServiceImpl extends ServiceImpl<ActivationRecordMapper, ActivationRecord> implements IActivationRecordService {
    @Autowired
    public ActivationRecordMapper activationRecordMapper;
    @Autowired
    public IDeviceActivationService deviceActivationService;

    @Override
    public void updateDelFlag(String id) {
            activationRecordMapper.updateDelFlag(id);
    }

    @Override
    public String orderNo() {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String nowDay = sdf.format(new Date());
        int newNum= activationRecordMapper.getCount();
        //编码为5位
        String newString = String.format("%05d", newNum);

        return nowDay+newString;
    }

    @Override
    public int getCount() {
        return activationRecordMapper.getCount();
    }

    @Override
    public ActivationRecord findByOrderNo(String orderNo) {
        QueryWrapper cq = new QueryWrapper(ActivationRecord.class);
        cq.eq(true, "orderNo", orderNo);
        return getOne(cq);
    }

    @Override
    public Boolean paySuccess(String outTradeNo, Map<String, Object> context) {
        ActivationRecord activationRecord = findByOrderNo(outTradeNo);
        if (oConvertUtils.isNotEmpty(activationRecord)) {
            Integer payStatus = activationRecord.getPayState();
            String payTime = DateUtils.getDateTime();
            if (payStatus.equals(PayStatusEnum.NOPAY.getCode())) {
                UpdateWrapper update = new UpdateWrapper();
                update.set(true, "payState", PayStatusEnum.PAY.code);
                update.set(true, "payTime", payTime);

                //update.set(true, "orderState", orderState);
                update.set(true, "payType", context.get("payType"));
                update.eq(true, "id", activationRecord.getId());
                update(update);
            }

            DeviceActivation deviceActivation=deviceActivationService.getById(activationRecord.getDeviceId());
            Date activationStart = DateUtil.parse(DateUtil.today());
            Date activationEnd = DateUtil.offsetDay(activationStart,activationRecord.getActivationDuration());
            deviceActivation.setUseStatus("2");
            deviceActivation.setActivationStartTime(activationStart);
            deviceActivation.setActivationEndTime(activationEnd);
            deviceActivationService.updateById(deviceActivation);
        }

        return true;
    }
}

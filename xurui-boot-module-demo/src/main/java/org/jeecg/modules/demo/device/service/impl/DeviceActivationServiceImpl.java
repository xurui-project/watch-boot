package org.jeecg.modules.demo.device.service.impl;

import org.jeecg.modules.demo.device.entity.DeviceActivation;
import org.jeecg.modules.demo.device.mapper.DeviceActivationMapper;
import org.jeecg.modules.demo.device.service.IDeviceActivationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.Date;
import java.util.List;

/**
 * @Description: device_activation
 * @Author: xurui-boot
 * @Date:   2022-01-18
 * @Version: V1.0
 */
@Service
public class DeviceActivationServiceImpl extends ServiceImpl<DeviceActivationMapper, DeviceActivation> implements IDeviceActivationService {
    @Autowired
    public DeviceActivationMapper deviceActivationMapper;

    @Override
    public DeviceActivation deviceStatus(String deviceId) {
        return deviceActivationMapper.deviceStatus(deviceId);
    }

    @Override
    public int countTrialEnd(String trialEndTime, String activationEndTime) {
        return deviceActivationMapper.countTrialEnd(trialEndTime,activationEndTime);
    }

    @Override
    public List<DeviceActivation> getCustomerList(String customerId) {
        return deviceActivationMapper.getCustomerList(customerId);
    }

    @Override
    public void updateDelFlag(String id) {
        deviceActivationMapper.updateDelFlag(id);
    }
}

package org.jeecg.modules.demo.test.service.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.jeecg.modules.demo.test.entity.XuRuiOrderCustomer;
import org.jeecg.modules.demo.test.entity.XuRuiOrderMain;
import org.jeecg.modules.demo.test.entity.XuRuiOrderTicket;
import org.jeecg.modules.demo.test.mapper.XuRuiOrderCustomerMapper;
import org.jeecg.modules.demo.test.mapper.XuRuiOrderMainMapper;
import org.jeecg.modules.demo.test.mapper.XuRuiOrderTicketMapper;
import org.jeecg.modules.demo.test.service.IXuRuiOrderMainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 订单
 * @Author: xurui-boot
 * @Date:  2019-02-15
 * @Version: V1.0
 */
@Service
public class XuRuiOrderMainServiceImpl extends ServiceImpl<XuRuiOrderMainMapper, XuRuiOrderMain> implements IXuRuiOrderMainService {

    @Autowired
    private XuRuiOrderMainMapper xuRuiOrderMainMapper;
    @Autowired
    private XuRuiOrderCustomerMapper xuRuiOrderCustomerMapper;
    @Autowired
    private XuRuiOrderTicketMapper xuRuiOrderTicketMapper;

    @Override
    @Transactional
    public void saveMain(XuRuiOrderMain xuRuiOrderMain, List<XuRuiOrderCustomer> xuRuiOrderCustomerList, List<XuRuiOrderTicket> xuRuiOrderTicketList) {
        xuRuiOrderMainMapper.insert(xuRuiOrderMain);
        if (xuRuiOrderCustomerList != null) {
            for (XuRuiOrderCustomer entity : xuRuiOrderCustomerList) {
                entity.setOrderId(xuRuiOrderMain.getId());
                xuRuiOrderCustomerMapper.insert(entity);
            }
        }
        if (xuRuiOrderTicketList != null) {
            for (XuRuiOrderTicket entity : xuRuiOrderTicketList) {
                entity.setOrderId(xuRuiOrderMain.getId());
                xuRuiOrderTicketMapper.insert(entity);
            }
        }
    }

    @Override
    @Transactional
    public void updateMain(XuRuiOrderMain xuRuiOrderMain, List<XuRuiOrderCustomer> xuRuiOrderCustomerList, List<XuRuiOrderTicket> xuRuiOrderTicketList) {
        xuRuiOrderMainMapper.updateById(xuRuiOrderMain);

        //1.先删除子表数据
        xuRuiOrderTicketMapper.deleteTicketsByMainId(xuRuiOrderMain.getId());
        xuRuiOrderCustomerMapper.deleteCustomersByMainId(xuRuiOrderMain.getId());

        //2.子表数据重新插入
        if (xuRuiOrderCustomerList != null) {
            for (XuRuiOrderCustomer entity : xuRuiOrderCustomerList) {
                entity.setOrderId(xuRuiOrderMain.getId());
                xuRuiOrderCustomerMapper.insert(entity);
            }
        }
        if (xuRuiOrderTicketList != null) {
            for (XuRuiOrderTicket entity : xuRuiOrderTicketList) {
                entity.setOrderId(xuRuiOrderMain.getId());
                xuRuiOrderTicketMapper.insert(entity);
            }
        }
    }

    /**
     * 一对多维护逻辑改造  LOWCOD-315
     * @param xuRuiOrderMain
     * @param xuRuiOrderCustomerList
     * @param xuRuiOrderTicketList
     */
    @Override
    @Transactional
    public void updateCopyMain(XuRuiOrderMain xuRuiOrderMain, List<XuRuiOrderCustomer> xuRuiOrderCustomerList, List<XuRuiOrderTicket> xuRuiOrderTicketList) {
        xuRuiOrderMainMapper.updateById(xuRuiOrderMain);

        // 循环前台传过来的数据
        for (XuRuiOrderTicket ticket:xuRuiOrderTicketList){
            // 先查询子表数据库
            XuRuiOrderTicket orderTicket = xuRuiOrderTicketMapper.selectById(ticket.getId());
            if(orderTicket == null){
                // 当传过来的id数据库不存在时，说明数据库没有，走新增逻辑
                ticket.setOrderId(xuRuiOrderMain.getId());
                xuRuiOrderTicketMapper.insert(ticket);
                break;
            }
            if(orderTicket.getId().equals(ticket.getId())){
                // 传过来的id和数据库id一至时，说明数据库存在该数据，走更新逻辑
                xuRuiOrderTicketMapper.updateById(ticket);
            }
        }
        for (XuRuiOrderCustomer customer:xuRuiOrderCustomerList){
            // 先查询子表数据库
            XuRuiOrderCustomer customers = xuRuiOrderCustomerMapper.selectById(customer.getId());
            if(customers == null){
                // 当传过来的id数据库不存在时，说明数据库没有，走新增逻辑
                customer.setOrderId(xuRuiOrderMain.getId());
                xuRuiOrderCustomerMapper.insert(customer);
                break;
            }
            if(customers.getId().equals(customer.getId())){
                //TODO 传过来的id和数据库id一至时，说明数据库存在该数据，走更新逻辑
                xuRuiOrderCustomerMapper.updateById(customer);
            }
        }
        // 当跟新和删除之后取差集， 当传过来的id不存在，而数据库存在时，说明已删除，走删除逻辑
        List<XuRuiOrderTicket> xuRuiOrderTickets = xuRuiOrderTicketMapper.selectTicketsByMainId(xuRuiOrderMain.getId());
        List<XuRuiOrderTicket> collect = xuRuiOrderTickets.stream()
                .filter(item -> !xuRuiOrderTicketList.stream()
                .map(e -> e.getId())
                .collect(Collectors.toList())
                .contains(item.getId()))
                .collect(Collectors.toList());
        // for循环删除id
        for (XuRuiOrderTicket ticket:collect){
            xuRuiOrderTicketMapper.deleteById(ticket.getId());
        }

        List<XuRuiOrderCustomer> xuRuiOrderCustomers = xuRuiOrderCustomerMapper.selectCustomersByMainId(xuRuiOrderMain.getId());
        List<XuRuiOrderCustomer> customersCollect = xuRuiOrderCustomers.stream()
                .filter(item -> !xuRuiOrderCustomerList.stream()
                        .map(e -> e.getId())
                        .collect(Collectors.toList())
                        .contains(item.getId()))
                .collect(Collectors.toList());
        //TODO for循环删除id
        for (XuRuiOrderCustomer c:customersCollect){
            xuRuiOrderCustomerMapper.deleteById(c.getId());
        }
    }
	@Override
	@Transactional
	public void delMain(String id) {
        xuRuiOrderMainMapper.deleteById(id);
        xuRuiOrderTicketMapper.deleteTicketsByMainId(id);
        xuRuiOrderCustomerMapper.deleteCustomersByMainId(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
            xuRuiOrderMainMapper.deleteById(id);
            xuRuiOrderTicketMapper.deleteTicketsByMainId(id.toString());
            xuRuiOrderCustomerMapper.deleteCustomersByMainId(id.toString());
		}
	}

}

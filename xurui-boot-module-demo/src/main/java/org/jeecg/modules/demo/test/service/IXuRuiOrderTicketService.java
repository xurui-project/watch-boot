package org.jeecg.modules.demo.test.service;

import java.util.List;

import org.jeecg.modules.demo.test.entity.XuRuiOrderTicket;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 订单机票
 * @Author: xurui-boot
 * @Date:  2019-02-15
 * @Version: V1.0
 */
public interface IXuRuiOrderTicketService extends IService<XuRuiOrderTicket> {
	
	public List<XuRuiOrderTicket> selectTicketsByMainId(String mainId);
}

package org.jeecg.modules.demo.device.service;

import org.jeecg.modules.demo.device.entity.DeviceConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: device_config
 * @Author: xurui-boot
 * @Date:   2022-01-18
 * @Version: V1.0
 */
public interface IDeviceConfigService extends IService<DeviceConfig> {

    public  DeviceConfig quertyConfigInfo();

    public void updateDelFlag(String id);
}

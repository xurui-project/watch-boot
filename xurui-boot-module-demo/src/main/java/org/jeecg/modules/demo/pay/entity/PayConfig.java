package org.jeecg.modules.demo.pay.entity;


import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.egzosn.pay.common.bean.MsgType;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.system.base.entity.XuRuiEntity;
import org.jeecg.modules.demo.pay.enums.PayType;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: pay_config
 * @Author: xurui-boot
 * @Date:   2022-01-17
 * @Version: V1.0
 */
@Data
@TableName("pay_config")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="pay_config对象", description="pay_config")
public class PayConfig extends XuRuiEntity {
    private static final long serialVersionUID = 1L;


	/**账户名称*/
	@Excel(name = "账户名称", width = 15)
    @ApiModelProperty(value = "账户名称")
    private java.lang.String payName;
	/**编码*/
	@Excel(name = "编码", width = 15)
    @ApiModelProperty(value = "编码")
    private java.lang.String payCode;
	/**支付合作id,商户id，差不多是支付平台的账号或id*/
	@Excel(name = "支付合作id,商户id，差不多是支付平台的账号或id", width = 15)
    @ApiModelProperty(value = "支付合作id,商户id，差不多是支付平台的账号或id")
    private java.lang.String partner;
	/**应用id*/
	@Excel(name = "应用id", width = 15)
    @ApiModelProperty(value = "应用id")
    private java.lang.String appId;
    /**平台类型*/
    @Excel(name = "平台类型", width = 15)
    @ApiModelProperty(value = "平台类型(用于区分 微信小程序支付和微信公众号支付)")
    @Dict(dicCode ="platType" )
    private String platType;
	/**支付平台公钥(签名校验使用)，sign_type只有单一key时public_key与private_key相等，比如sign_type=MD5(友店支付除外)的情况*/
	@Excel(name = "支付平台公钥(签名校验使用)，sign_type只有单一key时public_key与private_key相等，比如sign_type=MD5(友店支付除外)的情况", width = 15)
    @ApiModelProperty(value = "支付平台公钥(签名校验使用)，sign_type只有单一key时public_key与private_key相等，比如sign_type=MD5(友店支付除外)的情况")
    private java.lang.String publicKey;
	/**应用私钥(生成签名)*/
	@Excel(name = "应用私钥(生成签名)", width = 15)
    @ApiModelProperty(value = "应用私钥(生成签名)")
    private java.lang.String privateKey;
	/**异步回调地址*/
	@Excel(name = "异步回调地址", width = 15)
    @ApiModelProperty(value = "异步回调地址")
    private java.lang.String notifyUrl;
	/**同步回调地址*/
	@Excel(name = "同步回调地址", width = 15)
    @ApiModelProperty(value = "同步回调地址")
    private java.lang.String returnUrl;
	/**收款账号, 针对支付宝*/
	@Excel(name = "收款账号, 针对支付宝", width = 15)
    @ApiModelProperty(value = "收款账号, 针对支付宝")
    private java.lang.String seller;
	/**签名类型*/
	@Excel(name = "签名类型", width = 15)
    @ApiModelProperty(value = "签名类型")
    private java.lang.String signType;
	/**枚举值，字符编码 utf-8,gbk等等*/
	@Excel(name = "枚举值，字符编码 utf-8,gbk等等", width = 15)
    @ApiModelProperty(value = "枚举值，字符编码 utf-8,gbk等等")
    private java.lang.String inputCharset;
	/**支付类型,aliPay：支付宝，wxPay：微信, youdianPay: 友店微信,此处开发者自定义对应com.egzosn.pay.demo.entity.PayType枚举值*/
	@Excel(name = "支付类型,aliPay：支付宝，wxPay：微信, youdianPay: 友店微信,此处开发者自定义对应com.egzosn.pay.demo.entity.PayType枚举值", width = 15)
    @ApiModelProperty(value = "支付类型,aliPay：支付宝，wxPay：微信, youdianPay: 友店微信,此处开发者自定义对应com.egzosn.pay.demo.entity.PayType枚举值")
    private PayType payType;
	/**消息类型，text,xml,json*/
	@Excel(name = "消息类型，text,xml,json", width = 15)
    @ApiModelProperty(value = "消息类型，text,xml,json")
    private MsgType msgType;
	/**请求证书地址，请使用绝对路径*/
	@Excel(name = "请求证书地址，请使用绝对路径", width = 15)
    @ApiModelProperty(value = "请求证书地址，请使用绝对路径")
    private java.lang.String keystorePath;
	/**证书对应的密码*/
	@Excel(name = "证书对应的密码", width = 15)
    @ApiModelProperty(value = "证书对应的密码")
    private java.lang.String storePassword;
	/**是否为测试环境*/
	@Excel(name = "是否为测试环境", width = 15)
    @ApiModelProperty(value = "是否为测试环境")
    private Boolean isTest;
	/**是否启用*/
	@Excel(name = "是否启用", width = 15)
    @ApiModelProperty(value = "是否启用")
    @Dict(dicCode = "payStatus")
    private java.lang.Integer payStatus;
	/**applinks*/
	@Excel(name = "applinks", width = 15)
    @ApiModelProperty(value = "applinks")
    private java.lang.String applinks;
	/**logo*/
	@Excel(name = "logo", width = 15)
    @ApiModelProperty(value = "logo")
    private java.lang.String payLogo;
	/**乐观锁*/
	@Excel(name = "乐观锁", width = 15)
    @ApiModelProperty(value = "乐观锁")
    private java.lang.String revision;
//	/**创建人*/
//    @ApiModelProperty(value = "创建人")
//    private java.lang.String createBy;
//	/**创建时间*/
//	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
//    @DateTimeFormat(pattern="yyyy-MM-dd")
//    @ApiModelProperty(value = "创建时间")
//    private java.util.Date createTime;
//	/**更新人*/
//    @ApiModelProperty(value = "更新人")
//    private java.lang.String updateBy;
//	/**更新时间*/
//	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
//    @DateTimeFormat(pattern="yyyy-MM-dd")
//    @ApiModelProperty(value = "更新时间")
//    private java.util.Date updateTime;
    /**
     * 删除状态（0，正常，1已删除）
     */
    @Excel(name = "删除状态", width = 15,dicCode="del_flag")
    @TableLogic
    private String delFlag;;
}

package org.jeecg.modules.demo.device.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.device.entity.DeviceConfig;
import org.jeecg.modules.demo.device.service.IDeviceConfigService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.XuRuiController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: device_config
 * @Author: xurui-boot
 * @Date:   2022-01-18
 * @Version: V1.0
 */
@Api(tags="device_config")
@RestController
@RequestMapping("/device/deviceConfig")
@Slf4j
public class DeviceConfigController extends XuRuiController<DeviceConfig, IDeviceConfigService> {
	@Autowired
	private IDeviceConfigService deviceConfigService;
	
	/**
	 * 分页列表查询
	 *
	 * @param deviceConfig
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "device_config-分页列表查询")
	@ApiOperation(value="device_config-分页列表查询", notes="device_config-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(DeviceConfig deviceConfig,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<DeviceConfig> queryWrapper = QueryGenerator.initQueryWrapper(deviceConfig, req.getParameterMap());
		Page<DeviceConfig> page = new Page<DeviceConfig>(pageNo, pageSize);
		IPage<DeviceConfig> pageList = deviceConfigService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param deviceConfig
	 * @return
	 */
	@AutoLog(value = "device_config-添加")
	@ApiOperation(value="device_config-添加", notes="device_config-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody DeviceConfig deviceConfig) {
		deviceConfigService.save(deviceConfig);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param deviceConfig
	 * @return
	 */
	@AutoLog(value = "device_config-编辑")
	@ApiOperation(value="device_config-编辑", notes="device_config-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody DeviceConfig deviceConfig) {
		deviceConfigService.updateById(deviceConfig);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "device_config-通过id删除")
	@ApiOperation(value="device_config-通过id删除", notes="device_config-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		//deviceConfigService.removeById(id);
		deviceConfigService.updateDelFlag(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "device_config-批量删除")
	@ApiOperation(value="device_config-批量删除", notes="device_config-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.deviceConfigService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "device_config-通过id查询")
	@ApiOperation(value="device_config-通过id查询", notes="device_config-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		DeviceConfig deviceConfig = deviceConfigService.getById(id);
		if(deviceConfig==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(deviceConfig);
	}


	 @AutoLog(value = "device_config-通过id查询")
	 @ApiOperation(value="device_config-通过id查询", notes="device_config-通过id查询")
	 @GetMapping(value = "/quertyConfigInfo")
	 public Result<?> quertyConfigInfo() {
		 DeviceConfig deviceConfig = deviceConfigService.quertyConfigInfo();
		 if(deviceConfig==null) {
			 return Result.error("未找到对应数据");
		 }
		 return Result.OK(deviceConfig);
	 }

    /**
    * 导出excel
    *
    * @param request
    * @param deviceConfig
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, DeviceConfig deviceConfig) {
        return super.exportXls(request, deviceConfig, DeviceConfig.class, "device_config");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, DeviceConfig.class);
    }

}

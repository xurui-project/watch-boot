package org.jeecg.modules.demo.pay.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: pay_info
 * @Author: xurui-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
@Data
@TableName("pay_info")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="pay_info对象", description="pay_info")
public class PayInfo implements Serializable {
    private static final long serialVersionUID = 1L;

	/**订单ID*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "订单ID")
    private java.lang.String id;
	/**对应业务Id*/
	@Excel(name = "对应业务Id", width = 15)
    @ApiModelProperty(value = "对应业务Id")
    private java.lang.String deviceId;
    @ApiModelProperty(value = "支付流水标题")
    private String orderTitle;

    @ApiModelProperty(value = "支付流水描述")
    private String orderBody;
	/**订单金额*/
	@Excel(name = "订单金额", width = 15)
    @ApiModelProperty(value = "订单金额")
    private java.math.BigDecimal activationCost;
	/**支付状态*/
	@Excel(name = "支付状态", width = 15)
    @ApiModelProperty(value = "支付状态")
    private java.lang.Integer payStatus;
	/**支付方式*/
	@Excel(name = "支付方式", width = 15)
    @ApiModelProperty(value = "支付方式")
    private java.lang.String payType;
	/**支付终端*/
	@Excel(name = "支付终端", width = 15)
    @ApiModelProperty(value = "支付终端")
    private java.lang.String terminalType;
	/**支付时间*/
	@Excel(name = "支付时间", width = 15)
    @ApiModelProperty(value = "支付时间")
    private java.lang.String payDate;
	/**退款日期*/
	@Excel(name = "退款日期", width = 15)
    @ApiModelProperty(value = "退款日期")
    private java.lang.String returnDate;
	/**退款金额*/
	@Excel(name = "退款金额", width = 15)
    @ApiModelProperty(value = "退款金额")
    private java.math.BigDecimal returnAmount;
	/**退款原因*/
	@Excel(name = "退款原因", width = 15)
    @ApiModelProperty(value = "退款原因")
    private java.lang.String reason;
	/**发票ID*/
	@Excel(name = "发票ID", width = 15)
    @ApiModelProperty(value = "发票ID")
    private java.lang.String invoiceId;
	/**第三方订单号*/
	@Excel(name = "第三方订单号", width = 15)
    @ApiModelProperty(value = "第三方订单号")
    private java.lang.String outTradeNo;
	/**关闭日期*/
	@Excel(name = "关闭日期", width = 15)
    @ApiModelProperty(value = "关闭日期")
    private java.lang.String closeDate;
	/**payConfigId*/
	@Excel(name = "payConfigId", width = 15)
    @ApiModelProperty(value = "payConfigId")
    private java.lang.String payConfigId;

    @ApiModelProperty(value = "支付账户")
    private String fromPay;

    @ApiModelProperty(value = "支付终端")
    private String transactionType;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
	/**是否删除(0未删除 1已删除)*/
	@Excel(name = "是否删除(0未删除 1已删除)", width = 15)
    @ApiModelProperty(value = "是否删除(0未删除 1已删除)")
    private java.lang.Integer delFlag;
}

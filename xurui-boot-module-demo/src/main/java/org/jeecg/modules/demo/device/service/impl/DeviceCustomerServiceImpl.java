package org.jeecg.modules.demo.device.service.impl;

import org.jeecg.modules.demo.device.entity.DeviceCustomer;
import org.jeecg.modules.demo.device.mapper.DeviceCustomerMapper;
import org.jeecg.modules.demo.device.service.IDeviceCustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: device_customer
 * @Author: xurui-boot
 * @Date:   2022-01-18
 * @Version: V1.0
 */
@Service
public class DeviceCustomerServiceImpl extends ServiceImpl<DeviceCustomerMapper, DeviceCustomer> implements IDeviceCustomerService {
    @Autowired
    public DeviceCustomerMapper deviceCustomerMapper;

    @Override
    public void updateDelFlag(String id) {
        deviceCustomerMapper.updateDelFlag(id);
    }
}

package org.jeecg.modules.demo.device.service.impl;

import org.jeecg.modules.demo.device.entity.DeviceConfig;
import org.jeecg.modules.demo.device.mapper.DeviceConfigMapper;
import org.jeecg.modules.demo.device.service.IDeviceConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: device_config
 * @Author: xurui-boot
 * @Date:   2022-01-18
 * @Version: V1.0
 */
@Service
public class DeviceConfigServiceImpl extends ServiceImpl<DeviceConfigMapper, DeviceConfig> implements IDeviceConfigService {
    @Autowired
    public DeviceConfigMapper deviceConfigMapper;

    @Override
    public DeviceConfig quertyConfigInfo() {
        return deviceConfigMapper.quertyConfigInfo();
    }

    @Override
    public void updateDelFlag(String id) {
        deviceConfigMapper.updateDelFlag(id);
    }
}

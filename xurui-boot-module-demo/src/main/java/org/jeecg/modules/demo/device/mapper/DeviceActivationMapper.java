package org.jeecg.modules.demo.device.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.demo.device.entity.DeviceActivation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: device_activation
 * @Author: xurui-boot
 * @Date:   2022-01-18
 * @Version: V1.0
 */
public interface DeviceActivationMapper extends BaseMapper<DeviceActivation> {

    public DeviceActivation deviceStatus(@Param("deviceId") String deviceId);

    public int countTrialEnd(@Param("trialEndTime") String trialEndTime, @Param("activationEndTime") String activationEndTime);

    public List<DeviceActivation> getCustomerList(String customerId);

    public void updateDelFlag(@Param("id") String id);
}

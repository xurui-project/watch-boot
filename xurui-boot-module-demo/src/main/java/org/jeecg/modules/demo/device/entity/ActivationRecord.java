package org.jeecg.modules.demo.device.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: activation_record
 * @Author: xurui-boot
 * @Date:   2022-01-18
 * @Version: V1.0
 */
@Data
@TableName("activation_record")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="activation_record对象", description="activation_record")
public class ActivationRecord implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键ID*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private String id;
	/**设备ID*/
	@Excel(name = "设备ID", width = 15)
    @ApiModelProperty(value = "设备ID")
    private String deviceId;
	/**激活终端类型(1.Android 2.IOS)*/
	@Excel(name = "激活终端类型(1.Android 2.IOS)", width = 15)
    @ApiModelProperty(value = "激活终端类型(1.Android 2.IOS)")
    @Dict(dicCode = "terminal_type")
    private String terminalType;
	/**激活时间*/
	@Excel(name = "激活时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "激活时间")
    private Date activationTime;
	/**激活费用*/
	@Excel(name = "激活费用", width = 15)
    @ApiModelProperty(value = "激活费用")
    private BigDecimal activationCost;
	/**激活时长*/
	@Excel(name = "激活时长", width = 15)
    @ApiModelProperty(value = "激活时长")
    private Integer activationDuration;
    /**激活时长*/
    @Excel(name = "支付信息ID", width = 15)
    @ApiModelProperty(value = "支付信息ID")
    private String payInfoId;
    /**激活时长*/
    @Excel(name = "支付方式", width = 15)
    @ApiModelProperty(value = "支付方式(aliPay：支付宝，wxPay：微信, ios: 苹果,此处开发者自定义对应com.egzosn.pay.demo.entity.PayType枚举值)")
    @Dict(dicCode = "payType")
    private String payType;
    /**激活时长*/
    @Excel(name = "付款日期", width = 15)
    @ApiModelProperty(value = "付款日期")
    private Date payTime;
    /**激活时长*/
    @Excel(name = "支付状态", width = 15)
    @ApiModelProperty(value = "支付状态")
    @Dict(dicCode = "payState")
    private Integer payState;

    @ApiModelProperty(value = "订单编号")
    private String orderNo;

	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
    /**
     * 删除状态（0，正常，1已删除）
     */
    @Excel(name = "删除状态", width = 15,dicCode="del_flag")
    @TableLogic
    private String delFlag;
}

package org.jeecg.modules.demo.device.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.demo.device.entity.ActivationRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: activation_record
 * @Author: xurui-boot
 * @Date:   2022-01-18
 * @Version: V1.0
 */
public interface ActivationRecordMapper extends BaseMapper<ActivationRecord> {

    public void updateDelFlag(@Param("id") String id);

    public int getCount();

}

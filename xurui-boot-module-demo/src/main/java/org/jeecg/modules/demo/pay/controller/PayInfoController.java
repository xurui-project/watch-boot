package org.jeecg.modules.demo.pay.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.pay.entity.PayInfo;
import org.jeecg.modules.demo.pay.service.IPayInfoService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.XuRuiController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: pay_info
 * @Author: xurui-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
@Api(tags="pay_info")
@RestController
@RequestMapping("/pay/payInfo")
@Slf4j
public class PayInfoController extends XuRuiController<PayInfo, IPayInfoService> {
	@Autowired
	private IPayInfoService payInfoService;
	
	/**
	 * 分页列表查询
	 *
	 * @param payInfo
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "pay_info-分页列表查询")
	@ApiOperation(value="pay_info-分页列表查询", notes="pay_info-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(PayInfo payInfo,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<PayInfo> queryWrapper = QueryGenerator.initQueryWrapper(payInfo, req.getParameterMap());
		Page<PayInfo> page = new Page<PayInfo>(pageNo, pageSize);
		IPage<PayInfo> pageList = payInfoService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param payInfo
	 * @return
	 */
	@AutoLog(value = "pay_info-添加")
	@ApiOperation(value="pay_info-添加", notes="pay_info-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody PayInfo payInfo) {
		payInfoService.save(payInfo);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param payInfo
	 * @return
	 */
	@AutoLog(value = "pay_info-编辑")
	@ApiOperation(value="pay_info-编辑", notes="pay_info-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody PayInfo payInfo) {
		payInfoService.updateById(payInfo);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "pay_info-通过id删除")
	@ApiOperation(value="pay_info-通过id删除", notes="pay_info-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		payInfoService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "pay_info-批量删除")
	@ApiOperation(value="pay_info-批量删除", notes="pay_info-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.payInfoService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "pay_info-通过id查询")
	@ApiOperation(value="pay_info-通过id查询", notes="pay_info-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		PayInfo payInfo = payInfoService.getById(id);
		if(payInfo==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(payInfo);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param payInfo
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, PayInfo payInfo) {
        return super.exportXls(request, payInfo, PayInfo.class, "pay_info");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, PayInfo.class);
    }

}

package org.jeecg.modules.demo.device.controller;

import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.device.entity.ActivationRecord;
import org.jeecg.modules.demo.device.entity.DeviceActivation;
import org.jeecg.modules.demo.device.service.IActivationRecordService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.demo.device.service.IDeviceActivationService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.XuRuiController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: activation_record
 * @Author: xurui-boot
 * @Date:   2022-01-18
 * @Version: V1.0
 */
@Api(tags="activation_record")
@RestController
@RequestMapping("/device/activationRecord")
@Slf4j
public class ActivationRecordController extends XuRuiController<ActivationRecord, IActivationRecordService> {
	@Autowired
	private IActivationRecordService activationRecordService;

	 @Autowired
	 private IDeviceActivationService deviceActivationService;
	
	/**
	 * 分页列表查询
	 *
	 * @param activationRecord
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "activation_record-分页列表查询")
	@ApiOperation(value="activation_record-分页列表查询", notes="activation_record-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ActivationRecord activationRecord,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ActivationRecord> queryWrapper = QueryGenerator.initQueryWrapper(activationRecord, req.getParameterMap());
		Page<ActivationRecord> page = new Page<ActivationRecord>(pageNo, pageSize);
		IPage<ActivationRecord> pageList = activationRecordService.page(page, queryWrapper);
		return Result.OK(pageList);
	}

	 @GetMapping(value = "/deviceActivation")
	 public Result<Map<String,Object>> deviceActivation(@RequestParam(name="deviceId",required=true) String deviceId,@RequestParam(name="terminalType",required=true) String terminalType) {
		 Result<Map<String,Object>> result = new Result<>();
		 //Result<DeviceActivation> result = new Result<DeviceActivation>();
		 Map<String,Object> resMap = new HashMap<String,Object>();
		 DeviceActivation deviceActivation = deviceActivationService.deviceStatus(deviceId);
		 ActivationRecord activationRecord=new ActivationRecord();
		 activationRecord.setDeviceId(deviceId);

		 activationRecord.setActivationDuration(deviceActivation.getActivationDuration());
		 if("ios".equals(terminalType)){
			 activationRecord.setTerminalType("2");
			 activationRecord.setActivationCost(deviceActivation.getIosCost());
		 }else{
			 activationRecord.setTerminalType("1");
			activationRecord.setActivationCost(deviceActivation.getAndroidCost());
		 }
		 activationRecord.setActivationTime(new Date());
		 activationRecord.setPayState(0);
		 String orderNo=activationRecordService.orderNo();

		 activationRecord.setOrderNo(orderNo);
		activationRecordService.save(activationRecord);
		 resMap.put("activationRecord",activationRecord);
		 resMap.put("terminalType",terminalType);
		 result.setResult(resMap);
		 result.setMessage("已激活待支付");
		 result.setSuccess(true);
		 return result;
	 }

		 /**
          *   添加
          *
          * @param activationRecord
          * @return
          */
	@AutoLog(value = "activation_record-添加")
	@ApiOperation(value="activation_record-添加", notes="activation_record-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ActivationRecord activationRecord) {
		activationRecordService.save(activationRecord);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param activationRecord
	 * @return
	 */
	@AutoLog(value = "activation_record-编辑")
	@ApiOperation(value="activation_record-编辑", notes="activation_record-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ActivationRecord activationRecord) {
		activationRecordService.updateById(activationRecord);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "activation_record-通过id删除")
	@ApiOperation(value="activation_record-通过id删除", notes="activation_record-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		//activationRecordService.removeById(id);
		activationRecordService.updateDelFlag(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "activation_record-批量删除")
	@ApiOperation(value="activation_record-批量删除", notes="activation_record-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.activationRecordService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "activation_record-通过id查询")
	@ApiOperation(value="activation_record-通过id查询", notes="activation_record-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ActivationRecord activationRecord = activationRecordService.getById(id);
		if(activationRecord==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(activationRecord);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param activationRecord
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ActivationRecord activationRecord) {
        return super.exportXls(request, activationRecord, ActivationRecord.class, "activation_record");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ActivationRecord.class);
    }

}

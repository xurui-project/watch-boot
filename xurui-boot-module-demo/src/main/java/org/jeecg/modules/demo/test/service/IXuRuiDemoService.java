package org.jeecg.modules.demo.test.service;

import org.jeecg.common.system.base.service.XuRuiService;
import org.jeecg.modules.demo.test.entity.XuRuiDemo;

import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * @Description: jeecg 测试demo
 * @Author: xurui-boot
 * @Date:  2018-12-29
 * @Version: V1.0
 */
public interface IXuRuiDemoService extends XuRuiService<XuRuiDemo> {
	
	public void testTran();
	
	public XuRuiDemo getByIdCacheable(String id);
	
	/**
	 * 查询列表数据 在service中获取数据权限sql信息
	 * @param pageSize
	 * @param pageNo
	 * @return
	 */
	IPage<XuRuiDemo> queryListWithPermission(int pageSize, int pageNo);

	/**
	 * 根据用户权限获取导出字段
	 * @return
	 */
	String getExportFields();
}

package org.jeecg.modules.demo.pay.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.demo.pay.entity.PayInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: pay_info
 * @Author: xurui-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
public interface PayInfoMapper extends BaseMapper<PayInfo> {

}

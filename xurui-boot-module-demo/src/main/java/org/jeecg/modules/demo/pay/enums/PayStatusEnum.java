package org.jeecg.modules.demo.pay.enums;

import com.alibaba.fastjson.annotation.JSONType;
import com.baomidou.mybatisplus.annotation.IEnum;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zyf
 */
@JsonFormat(
        shape = JsonFormat.Shape.OBJECT
)
@JSONType(
        serializeEnumAsJavaBean = true
)
public enum PayStatusEnum implements IEnum {
    NOPAY(0, "未支付"),
    PAY(1, "已支付"),
    refund(2, "已退款");

    public String text;
    public Integer code;

    private PayStatusEnum(Integer code, String text) {
        this.text = text;
        this.code = code;
    }

    private PayStatusEnum(Integer code) {
        this.text = getText(code);
        this.code = code;
    }

    public static String getText(Object code) {
        String v = "";
        PayStatusEnum[] var2 = values();
        int var3 = var2.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            PayStatusEnum accountTypeEnum = var2[var4];
            if (accountTypeEnum.code.equals(code)) {
                v = accountTypeEnum.text;
                break;
            }
        }

        return v;
    }

    public static Map<String, Object> getMap() {
        Map<String, Object> map = new HashMap();
        PayStatusEnum[] var1 = values();
        int var2 = var1.length;

        for(int var3 = 0; var3 < var2; ++var3) {
            PayStatusEnum compNatureEnum = var1[var3];
            map.put(compNatureEnum.text, compNatureEnum.code);
        }

        return map;
    }

    public static PayStatusEnum getEnum(String code) {
        PayStatusEnum[] var1 = values();
        int var2 = var1.length;

        for(int var3 = 0; var3 < var2; ++var3) {
            PayStatusEnum payStatusEnum = var1[var3];
            if (payStatusEnum.code.equals(code)) {
                return payStatusEnum;
            }
        }

        return null;
    }

    public Serializable getValue() {
        return this.code;
    }

    public String getText() {
        return this.text;
    }

    public Integer getCode() {
        return this.code;
    }
}

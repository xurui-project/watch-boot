package org.jeecg.modules.demo.device.service;

import org.jeecg.modules.demo.device.entity.DeviceCustomer;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: device_customer
 * @Author: xurui-boot
 * @Date:   2022-01-18
 * @Version: V1.0
 */
public interface IDeviceCustomerService extends IService<DeviceCustomer> {

    public void updateDelFlag(String id);
}

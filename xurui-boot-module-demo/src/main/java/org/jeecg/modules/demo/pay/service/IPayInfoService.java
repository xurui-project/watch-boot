package org.jeecg.modules.demo.pay.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.demo.pay.entity.PayInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * @Description: pay_info
 * @Author: xurui-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
public interface IPayInfoService extends IService<PayInfo> {

    public Result<PayInfo> createPayInfo(Map params);

    /**
     * 获取支付信息
     *
     * @param params
     * @param tradeType 交易类型(可选值 APP,PAGE,NATIVE)
     * @return
     */
    Result<Map<String,Object>> getPayInfo(Map params, String tradeType);

    /**
     * 根据订单号查询订单
     *
     * @param outTradeNo
     * @return
     */
    PayInfo findByOutTradeNo(String outTradeNo);

    /**
     * 订单回调校验
     *
     * @param outTradeNo
     * @return
     */
    Boolean validate(String outTradeNo);

    /**
     * 订单支付成功处理
     *
     * @param context
     * @return
     */
    Boolean success(Map<String, Object> context);

}

package org.jeecg.modules.demo.pay.handler;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.egzosn.pay.common.api.PayService;
import com.egzosn.pay.common.bean.PayMessage;
import org.jeecg.modules.demo.device.entity.ActivationRecord;
import org.jeecg.modules.demo.device.service.IActivationRecordService;
import org.jeecg.modules.demo.pay.service.IPayInfoService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 商品支付处理器
 *
 * @author zyf
 */
@Component("orderPayHandler")
public class OrderPayHandler implements PaySuccessHandler{

    @Resource
    private IPayInfoService payInfoService;

    @Resource
    private IActivationRecordService activationRecordService;

    /**
     * 订单回调检验
     *
     * @param payMessage 支付回调消息
     * @param context    上下文参数,可传递到success方法内
     * @param payService 支付服务
     * @param outTradeNo 订单号
     * @return
     */
    @Override
    public boolean validate(PayMessage payMessage, Map<String, Object> context, PayService payService, String outTradeNo) {
        return payInfoService.validate(outTradeNo);
    }


    /**
     * 订单回调业务处理
     *
     * @param payMessage 支付回调消息
     * @param outTradeNo 订单号
     * @param context    上下文参数
     * @param payService 支付服务
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean success(PayMessage payMessage, String outTradeNo, Map<String, Object> context, PayService payService) {
        //获取创建订单时参数
        activationRecordService.paySuccess(outTradeNo, context);
        //ActivationRecord activationRecord = activationRecordService.findByOrderNo(outTradeNo);

        boolean b = payInfoService.success(context);

        return b;
    }
}

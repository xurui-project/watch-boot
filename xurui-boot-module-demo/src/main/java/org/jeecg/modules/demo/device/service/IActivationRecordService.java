package org.jeecg.modules.demo.device.service;

import org.jeecg.modules.demo.device.entity.ActivationRecord;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * @Description: activation_record
 * @Author: xurui-boot
 * @Date:   2022-01-18
 * @Version: V1.0
 */
public interface IActivationRecordService extends IService<ActivationRecord> {

    /**
     * 假删除
     * @param id
     */
    public void updateDelFlag(String id);

    /**
     * 获取订单单号
     * @return
     */
    public String orderNo();

    /**
     * 获取订单总数
     * @return
     */
    public int getCount();

    /**
     * 根据订单号查询订单
     *
     * @param orderNo
     * @return
     */
    ActivationRecord findByOrderNo(String orderNo);

    /**
     * 支付成功修改订单状态
     *
     * @param outTradeNo
     */
    Boolean paySuccess(String outTradeNo, Map<String, Object> context);
}

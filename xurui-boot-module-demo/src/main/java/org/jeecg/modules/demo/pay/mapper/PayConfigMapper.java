package org.jeecg.modules.demo.pay.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.demo.pay.entity.PayConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: pay_config
 * @Author: xurui-boot
 * @Date:   2022-01-17
 * @Version: V1.0
 */
public interface PayConfigMapper extends BaseMapper<PayConfig> {

}

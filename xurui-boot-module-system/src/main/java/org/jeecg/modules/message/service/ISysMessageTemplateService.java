package org.jeecg.modules.message.service;

import java.util.List;

import org.jeecg.common.system.base.service.XuRuiService;
import org.jeecg.modules.message.entity.SysMessageTemplate;

/**
 * @Description: 消息模板
 * @Author: xurui-boot
 * @Date:  2019-04-09
 * @Version: V1.0
 */
public interface ISysMessageTemplateService extends XuRuiService<SysMessageTemplate> {
    List<SysMessageTemplate> selectByCode(String code);
}

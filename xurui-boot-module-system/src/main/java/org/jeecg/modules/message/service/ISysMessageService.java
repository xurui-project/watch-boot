package org.jeecg.modules.message.service;

import org.jeecg.common.system.base.service.XuRuiService;
import org.jeecg.modules.message.entity.SysMessage;

/**
 * @Description: 消息
 * @Author: xurui-boot
 * @Date:  2019-04-09
 * @Version: V1.0
 */
public interface ISysMessageService extends XuRuiService<SysMessage> {

}

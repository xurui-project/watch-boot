package org.jeecg.common.api.vo;

import org.jeecg.common.util.ResultCode;

import java.util.HashMap;
import java.util.Objects;

/**
 * @author
 * @Description 返回结果信息
 * @date 2019/3/21 16:50
 */
public class AppleResult extends HashMap<Object, Object> {

    public static String CODE_KEY = "code";
    public static String MSG_KEY = "msg";
    public static String DATA_KEY = "data";


    /**
     * 调用结果(-1 异常 0 失败 1 成功)
     */
    public enum CallResult {
        /**
         * 异常
         */
        ERROR(-1, "异常"),
        /**
         * 失败
         */
        FAIL(0, "失败"),
        /**
         * 成功
         */
        SUCCESS(1, "成功");
        /**
         * code码
         */
        private Integer code;
        /**
         * 提示信息
         */
        private String msg;

        private CallResult(Integer code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public Integer getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 成功
     *
     * @return
     */
    public static AppleResult success() {
        AppleResult result = new AppleResult();
        result.put(CODE_KEY, AppleResult.CallResult.SUCCESS.getCode());
        result.put(MSG_KEY, AppleResult.CallResult.SUCCESS.getMsg());
        result.put(DATA_KEY, null);
        return result;
    }

    public static AppleResult success(String msg) {
        AppleResult result = success();
        result.put(MSG_KEY, msg);
        return result;
    }

    public static AppleResult success(ResultCode code, Object obj) {
        AppleResult result = success();
        result.put(CODE_KEY, code.code());
        result.put(MSG_KEY, code.msg());
        result.put(DATA_KEY, obj);
        return result;
    }

    public static AppleResult success(Object obj) {
        AppleResult result = success();
        result.put(DATA_KEY, obj);
        return result;
    }

    public static AppleResult success(String msg, Object obj) {
        AppleResult result = success();
        result.put(MSG_KEY, msg);
        result.put(DATA_KEY, obj);
        return result;
    }

    /**
     * 失败
     *
     * @return
     */
    public static AppleResult fail() {
        AppleResult result = new AppleResult();
        result.put(CODE_KEY, AppleResult.CallResult.FAIL.getCode());
        result.put(MSG_KEY, AppleResult.CallResult.FAIL.getMsg());
        result.put(DATA_KEY, null);
        return result;
    }

    public static AppleResult fail(String msg) {
        AppleResult result = fail();
        result.put(MSG_KEY, msg);
        return result;
    }

    public static AppleResult fail(ResultCode code, Object obj) {
        AppleResult result = fail();
        result.put(CODE_KEY, code.code());
        result.put(MSG_KEY, code.msg());
        result.put(DATA_KEY, obj);
        return result;
    }

    public static AppleResult fail(Object obj) {
        AppleResult result = fail();
        result.put(DATA_KEY, obj);
        return result;
    }

    public static AppleResult fail(String msg, Object obj) {
        AppleResult result = fail();
        result.put(MSG_KEY, msg);
        result.put(DATA_KEY, obj);
        return result;
    }

    /**
     * 异常
     *
     * @return
     */
    public static AppleResult error() {
        AppleResult result = new AppleResult();
        result.put(CODE_KEY, AppleResult.CallResult.ERROR.getCode());
        result.put(MSG_KEY, AppleResult.CallResult.ERROR.getMsg());
        result.put(DATA_KEY, null);
        return result;
    }

    public static AppleResult error(String msg) {
        AppleResult result = error();
        result.put(MSG_KEY, msg);
        return result;
    }

    public static AppleResult error(Object obj) {
        AppleResult result = error();
        result.put(DATA_KEY, obj);
        return result;
    }

    public static AppleResult error(String msg, Object obj) {
        AppleResult result = error();
        result.put(MSG_KEY, msg);
        result.put(DATA_KEY, obj);
        return result;
    }

    /**
     * 自定义
     *
     * @param code
     * @param msg
     * @param obj
     * @return
     */
    public static AppleResult custom(Object code, Object msg, Object obj) {
        AppleResult result = new AppleResult();
        result.put(CODE_KEY, code);
        result.put(MSG_KEY, msg);
        result.put(DATA_KEY, obj);
        return result;
    }

    /**
     * 自定义返回
     * @param code
     * @param obj
     * @return
     */
    public static AppleResult custom(ResultCode code, Object obj) {
        AppleResult result = new AppleResult();
        result.put(CODE_KEY, code.code());
        result.put(MSG_KEY, code.msg());
        result.put(DATA_KEY, obj);
        return result;
    }


    /**
     *
     * @return
     */
    public static AppleResult notLogin(){
        AppleResult result = new AppleResult();
        result.put(CODE_KEY, ResultCode.NOT_LOGIN.code());
        result.put(MSG_KEY, ResultCode.NOT_LOGIN.msg());
        result.put(DATA_KEY, null);
        return result;
    }

    /**
     * 验证成功
     * @param result
     * @return
     */
    public static boolean isSuccess(AppleResult result) {
        return !Objects.isNull(result.get(CODE_KEY)) && (Integer) result.get(CODE_KEY) == 1;
    }

}
